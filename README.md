# DEPRECATED
Please use https://gitlab.com/hukudo/devca instead.

# devcerts
Generates wildcard certificates for development on localhost
([details](1-docs/details.md)).


# Getting Started
See our [Getting Started Guide](1-docs/getting-started.md).


# Create a certificate:
```
./bin/devcerts-create myproject.localhost
./bin/devcerts-get myproject.localhost certs/
```
