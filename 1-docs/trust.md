# Trusting a Certificate Authority (CA)
Given a `ca.crt` file, here is how to trust it.

## Linux
### Debian / Ubuntu
```
sudo install --mode 644 ca.crt /usr/local/share/ca-certificates/devcerts.crt
sudo update-ca-certificates  # this creates a symlink in /etc/ssl/certs/devcerts.pem
```

### Arch Linux
```
sudo trust anchor --store ca.crt
```

## Mozilla NSS
Mozilla's [Network Security Services][nss] is used in [many open source
projects](nss-overview).
```
hash certutil || sudo apt-get install libnss3-tools
certutil -A -n devcerts -t "C,p,p" -i ca.crt -d sql:$HOME/.pki/nssdb
```

[nss]: https://developer.mozilla.org/en-US/docs/Mozilla/Projects/NSS
[nss-overview]: https://developer.mozilla.org/en-US/docs/Mozilla/Projects/NSS/Overview


## Firefox
Here is how to add a certificate authority manually. Note that you could also
use NSS (see above).

![Firefox: How to add a Certificate Authority][firefox-vid]

- open Preferences
- search "cert"
- click "View Certificates"
- in the "Authorities" tab
  - click Import and select `/etc/ssl/certs/devcerts.pem`
  - set "Trust this CA to identify websites"

[firefox-vid]: https://gitlab.com/hukudo/devenv/devcerts/-/wikis/uploads/5d46e33ed7a3e7ae9b800f93091e6151/firefox--how-to-add-a-certificate-authority.webm

## Chrome
- Settings > Manage Certificates > Authorities: Import
  - `/etc/ssl/certs/devcerts.pem`
  - "Trust this certificate for identifying websites"


# Removing Trust
```
certutil -d sql:$HOME/.pki/nssdb -D -n devcerts

sudo rm /usr/local/share/ca-certificates/devcerts.crt
sudo update-ca-certificates

curl https://foop.localhost
# SSL certificate problem: unable to get local issuer certificate
```

- open Preferences
- search "cert"
- click "View Certificates"
- in the "Authorities" tab
  - select "0-main devcerts Root CA"
  - click "Delete or Distrust"
  - confirm


