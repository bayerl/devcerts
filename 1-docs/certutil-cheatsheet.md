list
```
certutil -d sql:$HOME/.pki/nssdb -L
```

delete alias `devcerts`:
```
certutil -d sql:$HOME/.pki/nssdb -D -n devcerts
```
