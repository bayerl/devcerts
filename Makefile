.PHONY: default
default: build test

build: clean
	docker build -t devcerts .
	./bin/devcerts-ca-init
	./bin/devcerts-add foop.localhost

test:
	./bin/devcerts-ca-get-cert > ca.crt
	./bin/devcerts-trust-debian-ubuntu ca.crt
	docker cp devcerts-ca:/root/certs/ example/nginx/
	./example/nginx/main.sh
	curl https://foop.localhost

clean:
	# cleanup for easy roundtrips
	docker rm -f devcerts-ca || true
	docker volume rm devcerts_root || true
