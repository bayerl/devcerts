FROM nginx:1.17

RUN apt-get -qq update && apt-get -qqy install \
        bash \
        bash-completion \
        curl \
        jq \
        openssl \
        wget \
    && rm -rf /var/lib/apt/lists/*

# https://github.com/cloudflare/cfssl/releases
ARG CFSSL_URL=https://github.com/cloudflare/cfssl/releases/download/v1.4.1/cfssl_1.4.1_linux_amd64
RUN wget -q $CFSSL_URL -O /usr/local/bin/cfssl \
  && chmod +x /usr/local/bin/cfssl

WORKDIR /root

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

COPY generate-cert /usr/local/bin/generate-cert

ENTRYPOINT ["/entrypoint.sh"]
